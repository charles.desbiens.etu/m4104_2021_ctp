package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    // TODO Q1
    String salle;
    String poste;
    String DISTANCIEL;

    // TODO Q2.c
    SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
        poste = "";
        salle = DISTANCIEL;

        // TODO Q2.c
        model = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);

        // TODO Q4
        Spinner spinnerSalle = view.findViewById(R.id.spSalle);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(
                this.getContext(),
                R.array.list_salles,
                android.R.layout.simple_spinner_item
        );
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinnerSalle.setAdapter(adapter);

        Spinner spinnerPoste = view.findViewById(R.id.spPoste);

        ArrayAdapter adapterPoste = ArrayAdapter.createFromResource(
                this.getContext(),
                R.array.list_postes,
                android.R.layout.simple_spinner_item
        );
        // Specify the layout to use when the list of choices appears
        adapterPoste.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinnerPoste.setAdapter(adapterPoste);



        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            EditText login = getActivity().findViewById(R.id.tvLogin);
            model.setUsername(login.getText().toString());
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b
        update();

        spinnerSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                salle = arg0.getSelectedItem().toString();
                update();

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        spinnerPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                update();

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        // TODO Q9
    }

    // TODO Q5.a
    private void update() {
        Spinner spinnerPoste = getActivity().findViewById(R.id.spPoste);

        if(salle.equals("Distanciel")){
            spinnerPoste.setVisibility(View.INVISIBLE);
            spinnerPoste.setEnabled(false);
            model.setLocalisation("Distanciel");
        }
        else {
            spinnerPoste.setEnabled(true);
            spinnerPoste.setVisibility(View.VISIBLE);
            model.setLocalisation(salle + " : " + poste);
        }
    }
    // TODO Q9
}